﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;

namespace co.gamagi.web.assetbundle.ex.mvc4
{
    public static class BundleCollectionExt
    {
        public static BundleCollection AutoBundle(this BundleCollection collection)
        {
            var contentBase = HttpContext.Current.Server.MapPath("~/Content");
            var bundleBase = System.IO.Path.Combine(contentBase, "Bundles");
            var bundleNames = GetNumericSubDirectories(contentBase, "Bundles");

            foreach (var bundleName in bundleNames)
            {

                var thisBundle = System.IO.Path.Combine(bundleBase, bundleName);
                var scriptIncludes = CreateIncludeList(thisBundle, "Scripts");
                var stylesIncludes = CreateIncludeList(thisBundle, "Styles");

                var scriptBundle = new ScriptBundle(string.Format("~/bundles/{0}-js", bundleName));
                foreach (var scriptInclude in scriptIncludes)
                {
                    scriptBundle.Include(scriptInclude);
                }
                collection.Add(scriptBundle);

                var styleBundle = new StyleBundle(string.Format("~/bundles/{0}-css", bundleName));
                foreach (var styleInclude in stylesIncludes)
                {
                    styleBundle.Include(styleInclude);
                }
                collection.Add(styleBundle);
            }

            var setting = System.Configuration.ConfigurationManager.AppSettings.AllKeys.Where(w => w == "co.gamagi.web.assetbundle.Optimize").Select(s => System.Configuration.ConfigurationManager.AppSettings[s]).FirstOrDefault();
            BundleTable.EnableOptimizations = setting == null || setting.ToLower() != "off";

            setting = System.Configuration.ConfigurationManager.AppSettings.AllKeys.Where(w => w == "co.gamagi.web.assetbundle.Minify").Select(s => System.Configuration.ConfigurationManager.AppSettings[s]).FirstOrDefault();
            if (setting != null && setting.ToLower() == "off")
            {
                foreach (var bundle in collection)
                {
                    bundle.Transforms.Clear();
                }
            }

            return collection;
        }


        private static IEnumerable<string> CreateIncludeList(string contentBase, string subNode)
        {
            var subDirs = GetNumericSubDirectories(contentBase, subNode);
            Queue<string> filesList = new Queue<string>();
            foreach (var subDir in subDirs)
            {
                QueueUp(filesList, contentBase, subNode, subDir);
            }
            QueueUp(filesList, contentBase, subNode, "");
            return filesList;
        }

        private static Queue<string> QueueUp(Queue<string> q, string contentBase, string subNode, string subDir)
        {
            var files = GetFiles(contentBase, subNode, subDir);
            var baseUri = new Uri(contentBase);
            foreach (var file in files)
            {
                var virtualFilePath = "~/Content/Bundles/"  + baseUri.MakeRelativeUri(new Uri(file, UriKind.Absolute)).ToString();
                q.Enqueue(virtualFilePath);
            }
            return q;
        }

        private static IEnumerable<string> GetFiles(string contentBase, string subNode, string subDir)
        {
            var path = System.IO.Path.Combine(contentBase, subNode, subDir);
            var files = System.IO.Directory.GetFiles(path);
            return files;
        }

        private static IEnumerable<string> GetNumericSubDirectories(string contentBase, string subNode)
        {
            Uri u = new Uri(System.IO.Path.Combine(contentBase, subNode), UriKind.Absolute);
            var subdirectories = System.IO.Directory.GetDirectories(u.AbsolutePath).Select(s => u.MakeRelativeUri(new Uri(s)).ToString().Replace(subNode + "/", ""));
            return subdirectories;
        } 
    }
}