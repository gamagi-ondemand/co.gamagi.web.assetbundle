﻿using System.Web;
using System.Web.Mvc;

namespace co.gamagi.web.assetbundle.ex.mvc4
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}